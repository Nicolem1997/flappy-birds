package {
	import flash.display.MovieClip;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.events.Event;


	public class gameMain extends MovieClip {



		button_1.addEventListener(MouseEvent.CLICK, fl_ClickToLoadUnloadSWF);

		import fl.display.ProLoader;
		var fl_ProLoader: ProLoader;

		//This variable keeps track of whether you want to load or unload the SWF
		var fl_ToLoad: Boolean = true;

		function fl_ClickToLoadUnloadSWF(event: MouseEvent): void {
			if (fl_ToLoad) {
				fl_ProLoader = new ProLoader();
				fl_ProLoader.load(new URLRequest("floppyDog.fla/FB"));
				addChild(fl_ProLoader);
			} else {
			fl_ProLoader.unload();
				removeChild(fl_ProLoader);
		}
			// Toggle whether you want to load or unload the SWF
			fl_ToLoad = fl_ToLoad;
		}

		// constant variables
		const gravity: Number = 1;
		const space_pipes: Number = 300;
		const pipe_speed: Number = 8;
		const up_force: Number = 8;

		var player: Player = new Player();
		var lastpipe: Pipes = new Pipes();
		var pipes: Array = new Array();
		var speed: Number = 0;
		var score: Number = 0;

		public function gameMain() {
			init();
		}



		function init(): void {
			player = new Player();
			lastpipe = new Pipes();
			pipes = new Array();
			speed = 0;
			score = 0;

			// adding player
			player.x = stage.stageWidth / 2;
			player.y = stage.stageHeight / 2;
			addChild(player);

			// pipes
			createPipes();
			createPipes();
			createPipes();

			addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
			stage.addEventListener(KeyboardEvent.KEY_UP, key_up);
		}
		private function key_up(event: KeyboardEvent) {
			if (event.keyCode == Keyboard.SPACE) {
				speed = -up_force;
			}

		}
		function reset() {
			if (contains(player))
				removeChild(player);
			for (var i: int = 0; i < pipes.length; i++) {
				if (contains(pipes[i]) && pipes[i] != null)
					removeChild(pipes[i]);
				pipes[i] = null;
			}
			pipes.slice(0);
			init();
		}
		function onEnterFrameHandler(event: Event) {
			speed += gravity;
			player.y += speed;
			if (player.y + player.height / 2 > stage.stageHeight) {
				reset();
			}
			if (player.y - player.height / 2 < 0) {
				player.y = player.height / 2;
			}
			for (var i: int = 0; i < pipes.length; ++i) {
				updatePipes(i);
			}
			scoretxt.text = String(score);
		}
		function updatePipes(i: int) {
			var p: Pipes = pipes[i];

			if (p == null)
				return;
			p.x -= pipe_speed;

			if (p.x < -p.width) {
				changePipes(p);
			}
			// if dog hits pipe then play credits and restart the game
			if (p.hitTestPoint(player.x + player.width / 2, player.y + player.heigth / 2, true) || p.hitTestPoint(player.x - player.width / 2, player.y + player.height / 2, true) || p.hitTestPoint(player.x + player.width / 2, player.y - player.height / 2, true) || p.hitTestPoint(player.x - player.width / 2, player.y - player.height / 2, true)) {

				reset();
			}
			if ((player.x - player.width / 2 > p.x - p.width / 2) && !p.covered) {
				score++;
				p.covered = true;
			}
		}
		function changePipes(p: Pipes) {
			p.x = lastpipe.x + space_pipes;
			p.y = 100 + Math.random() * (stage.stageHeight - 200);
			lastpipe = p;
			p.covered = false;
		}
		function createPipes() {
			var p: Pipes = new Pipes();
			if (lastpipe.x == 0)
				p.x = 800;
			else
				p.x = lastpipe.x + space_pipes;
			p.y = 100 + Math.random() * (stage.stageHeight - 200);
			addChild(p);
			pipes.push(p);
			lastpipe = p;
		}

	}
}